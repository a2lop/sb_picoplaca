This is a validator for Quito's "Pico y Placa" System

## Steps

In the project directory, you can run:

### Cloning the repo

`git clone https://a2lop@bitbucket.org/a2lop/sb_picoplaca.git` to clone the repository

### Installing dependencies

`npm install` to install all the dependencies that are needed

### Run project

`npm run` to run the project

### Testing

`npm test` to run all test cases

## Credits

Andrés López
lopez.eandres@gmail.com
