import React from 'react'
import { render } from '@testing-library/react'
import App from './App'

test('check document elements', () => {
    const { getByTestId } = render(<App />)
    const inputElement = getByTestId('inputMask')
    expect(inputElement).toBeInTheDocument()

    const dateElement = getByTestId('inputDate')
    expect(dateElement).toBeInTheDocument()
})
