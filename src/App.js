import React from "react";
import "./styles/App.css";
import PlateValidator from "./components/PlateValidator";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <PlateValidator />
      </header>
    </div>
  );
}

export default App;
