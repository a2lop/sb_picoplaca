import React from 'react'
import InputMask from 'react-input-mask'
import DatePicker from 'react-datepicker'

import 'react-datepicker/dist/react-datepicker.css'
import '../styles/App.css'

class PlateValidator extends React.Component {
    constructor() {
        super()
        this.state = {
            plate: '',
            dateTs: new Date(),
            result: ''
        }
    }
    validate = () => {
        const dayOfTheWeek = new Date(this.state.dateTs).getDay()
        let result = ''
        if (dayOfTheWeek === 0 || dayOfTheWeek === 6) {
            result = 'There is NO "Pico y placa" at that time for that plate'
            this.setState({ result })
        } else {
            const timeFactor = new Date(this.state.dateTs).getHours() * 100 + new Date(this.state.dateTs).getMinutes()

            if (timeFactor < 700 || (timeFactor >= 930 && timeFactor <= 1600) || timeFactor > 1930) {
                result = 'There is NO "Pico y placa" at that time for that plate'
                this.setState({ result })
                return
            } else {
                const lastPlateNumber = this.state.plate.charAt(this.state.plate.length - 1)

                result =
                    Math.round(lastPlateNumber / 2) === dayOfTheWeek
                        ? '"Pico y placa" it\'s running at that time for that plate'
                        : 'There is NO "Pico y placa" at that time for that plate'
            }
        }

        this.setState({ result })
    }

    render() {
        return (
            <div>
                <InputMask
                    mask="aaa-9999"
                    maskChar=""
                    style={{ fontSize: 20 }}
                    value={this.state.plate}
                    onChange={event => this.setState({ plate: event.target.value })}
                    data-testid={'inputMask'}
                />

                <div>
                    <DatePicker
                        selected={this.state.dateTs}
                        onChange={date => this.setState({ dateTs: new Date(date).getTime() })}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={30}
                        timeCaption="time"
                        dateFormat="dd/MM/yyyy HH:mm aa"
                        customInput={<input style={{ fontSize: 20 }} disabled={true} data-testid={'inputDate'} />}
                        shouldCloseOnSelect={true}
                    />
                </div>
                <div>
                    <button className="main-button" onClick={this.validate}>
                        Check
                    </button>
                </div>
                {this.state.result !== '' && <span>{this.state.result}</span>}
            </div>
        )
    }
}

export default PlateValidator
